import pygame as pg
import time
import requests
import json
import tkinter as tk
import sys
import os

"""if getattr(sys, 'frozen', False):
	CurrentPath = sys._MEIPASS
else:
	CurrentPath = os.path.dirname(__file__)"""

class Frame():
    def __init__(self, Background, x, y, speed):
        self.background = Background
        self.x = x
        self.y = y
        self.speed = speed

    def draw(self):
        win.blit(self.background, (self.x, self.y))

    def add_x(self, val):
        self.x += val

    def radd_x(self, val):
        self.x -= val

class Subframe():
    def __init__(self, Frame, Gambar, x, y):
        self.frame = Frame
        self.background = Gambar
        self.x = Frame.x + x
        self.y = Frame.y + y
        self.speed = Frame.speed

    def draw(self):
        win.blit(self.background, (self.x, self.y))

class pushSlide():
    def __init__(self, Frame, gambar, list_gambar, x1, y1, x2, y2, threshold, pushSpeed):
        self.frame = Frame
        self.background = gambar
        self.list_bg = list_gambar
        self.x1 = x1
        self.anchorX = x1
        self.y1 = y1
        self.anchorY = y1
        self.x2 = x2
        self.anchorX2 = x2
        self.y2 = y2
        self.anchorY2 = y2
        self.threshold = threshold
        self.pushSpeed = pushSpeed
        self.openStat = False
        self.pushing_x = False
        self.pushing_xBack = True
        self.pushing_y = False
        self.mouseX = 0
        self.mouseY = 0
        self.mouseClick = 0
        self.idx = 0

    def pushX(self):
        selisih_x = self.x2 - self.anchorX
        if selisih_x >= 0:
            if self.x2 + self.threshold > self.x1 or self.x2 - self.threshold > self.x1:
                self.frame.add_x(self.pushSpeed)
                self.x1 += self.pushSpeed
            else:
                self.pushing_x = True
        if selisih_x < 0:
            if self.x2 + self.threshold < self.x1 or self.x2 - self.threshold < self.x1:
                self.x1 -= self.pushSpeed
                self.frame.radd_x(self.pushSpeed)
            else:
                self.pushing_x = True
    def pushX_back(self):
        selisih_x = self.anchorX - self.x2
        if selisih_x >= 0:
            if self.anchorX + self.threshold > self.x1 or self.anchorX - self.threshold > self.x1:
                self.frame.add_x(self.pushSpeed)
                self.x1 += self.pushSpeed
            else:
                self.pushing_xBack = True
                self.pushing_x = False
        if selisih_x < 0:
            if self.anchorX + self.threshold < self.x1 or self.anchorX - self.threshold < self.x1:
                self.x1 -= self.pushSpeed
                self.frame.radd_x(self.pushSpeed)
            else:
                self.pushing_xBack = True
                self.pushing_x = False
    def pushY(self):
        self.pushing_y = True
        if self.y2 + self.threshold > self.y1 or self.y2 - self.threshold > self.y1:
            self.y1 += self.pushSpeed
        elif self.y2 + self.threshold < self.y1 or self.y2 - self.threshold < self.y1:
            self.y1 -= self.pushSpeed

    def close(self):
        self.openStat = False

    def open(self):
        self.openStat = True

    def draw(self):
        if not self.openStat:
            if self.pushing_xBack:
                a = win.blit(self.background, (self.x1, self.y1))
                #print('ada')
                if a.collidepoint(self.mouseX, self.mouseY) and self.mouseClick == 1:
                    self.open()
                    print('hmm')
            else:
                self.pushX_back()
        else:
            if not self.pushing_x:
                a = win.blit(self.list_bg[self.idx], (self.x1, self.y1))
                if a.collidepoint(self.mouseX, self.mouseY) and self.mouseClick == 1:
                    self.pushing_x = True
                    self.pushing_xBack = False
            else:
                self.pushX()
                b = win.blit(self.list_bg[self.idx], (self.x1, self.y1))
                if not b.collidepoint(self.mouseX, self.mouseY) and self.mouseClick == 1:
                    self.close()
                if 33 < self.mouseX and self.mouseX < 130 and 35 < self.mouseY and self.mouseY < 55:
                    self.idx = 1
                    print('1')
                elif 33 < self.mouseX and self.mouseX < 130 and 75 < self.mouseY and self.mouseY < 95:
                    self.idx = 2
                    print('2')
                elif 33 < self.mouseX and self.mouseX < 130 and 115 < self.mouseY and self.mouseY < 135:
                    self.idx = 3
                    print('3')
                elif 33 < self.mouseX and self.mouseX < 130 and 155 < self.mouseY and self.mouseY < 175:
                    self.idx = 4
                    print('4')
                elif 33 < self.mouseX and self.mouseX < 130 and 195 < self.mouseY and self.mouseY < 215:
                    self.idx = 5
                    print('5')
                else:
                    self.idx = 0

class Tombol():
    def __init__(self, Frame, Gambar1, Gambar2, x, y):
        self.frame = Frame
        self.gambar1 = Gambar1
        self.gambar2 = Gambar2
        self.x = x + Frame.x
        self.y = y + Frame.x
        self.speed = frame2.speed

class print_teks():
    def __init__(self,frame, text, x, y, font, size, bold, italic, color):
        self.teks = pg.font.SysFont(font, size, bold, italic).render(text, 1, color)
        self.x = x + frame.x
        self.y = y + frame.y

    def draw(self):
        win.blit(self.teks, (self.x,self.y))

def switch_slide(slide_1, slide_2):
    global salamStat
    if slide_1.x <= screen_w:
        slide_1.x += slide_1.speed
    salamStat = True

def get_time():
    local = time.localtime()
    tahun = local[0]
    bulan = local[1]
    hari = local[2]
    return tahun, bulan, hari

def show_kota():
    list_kota = [0]
    try:
        url = "https://api.myquran.com/v1/sholat/kota/semua"
        req = requests.get(url)
        data = req.json()
        t = 0
        list_kota = []
        print("data",data)
        for i in data:
            namakota = i['lokasi']
            idkota = i['id']
            list_kota.append([i['lokasi'], i['id']])
            print("KOTA",list_kota[-1])
            t += 1
    except Exception as e:
        raise e
    return list_kota

def get_salahtime():
    global selected_kota
    subuh, imsak, terbit, dzuhur, ashar, maghrib, isya = 0,0,0,0,0,0,0
    tahun, bulan, hari = get_time()
    """selected_kota = 703
                tahun = 2020
                bulan = 8
                hari = 31"""
    if len(str(bulan)) == 1:
        bulan = f'0{bulan}'
    if len(str(hari)) == 1:
        hari = f'0{hari}'
    waktu = {}
    print("City:",selected_kota)
    # OLD API
    #print(f'https://api.banghasan.com/sholat/format/json/jadwal/kota/{int(selected_kota)}/tanggal/{tahun}-{bulan}-{hari}')
    # NEW API
    print(f'https://api.myquran.com/v1/sholat/jadwal/{selected_kota}/{tahun}/{bulan}/{hari}')
    try:    
        req = requests.get(f'https://api.myquran.com/v1/sholat/jadwal/{selected_kota}/{tahun}/{bulan}/{hari}')
        data = req.json()
        print("Data",data)
        waktu = data['data']['jadwal']
        subuh = waktu['subuh']
        imsak = waktu['imsak']
        terbit = waktu['terbit']
        dzuhur = waktu['dzuhur']
        ashar = waktu['ashar']
        maghrib = waktu['maghrib']
        isya = waktu['isya']
    except Exception as e:
        print(e)
    print(waktu)
    return subuh, imsak, terbit, dzuhur, ashar, maghrib, isya

def get_input():
    global input_kota, selected_kota, kota, run
    selected_kota = input_kota.get()
    try:
        selected_kota = f'{selected_kota[-6:-2]}'
    except:
        selected_kota = 123
        pass
    print(selected_kota)
    activate()
    #sys.exit()
    root.destroy()

def activate():
	global run
	run = True

def play_salam():
    pg.mixer.music.load(os.path.join(spritepath, 'salam1.mp3'))
    pg.mixer.music.play()

def drawWindow():
    win.blit(frame1.background, (frame1.x, frame1.y))
    frame2.draw()
    frame2_item1.draw()
    frame2_item2.draw()
    frame2_item3.draw()
    frame2_item4.draw()
    jam_subuh.draw()
    jam_dzuhur.draw()
    jam_ashar.draw()
    jam_maghrib.draw()
    jam_isya.draw()
    side_menu.draw()
    frame1.draw()

#==========================================TKINTER INPUT DATA
selected_kota = ''
root = tk.Tk()
root.geometry('300x80')
input_kota = tk.StringVar()
list_kota = show_kota()
input_kota.set(list_kota[0])
tk.OptionMenu(root,input_kota,*list_kota).pack()
tk.Button(root, text='SUBMIT',command=get_input).pack()
root.mainloop()

run = True
#=============================================window
pg.init()
pg.mixer.init()
subuh, imsak, terbit, dzuhur, ashar, maghrib, isya = get_salahtime()
screen_w = 600
screen_h = 500
win = pg.display.set_mode((screen_w, screen_h))
pg.display.set_caption('SHOLAT KUY')
clock = pg.time.Clock()

spritepath = os.getcwd()
spritepath = f'{spritepath}\data'
#pkl_name = os.path.join(folder,)
#spritepath = 'C:/Users/Danangjoyoo/Desktop/expython/adzan/data'

slide1 = pg.image.load(os.path.join(spritepath, 'initpage.png'))
slide2 = pg.image.load(os.path.join(spritepath, 'back1.png'))
slide2_item1 = pg.image.load(os.path.join(spritepath, 'tittle1.png'))
slide2_item2 = pg.image.load(os.path.join(spritepath, 'jadwal1.png'))
slide2_item3 = pg.image.load(os.path.join(spritepath, 'quotes1.png'))
slide2_item4 = pg.image.load(os.path.join(spritepath, 'tarasera1.png'))

side1 = pg.image.load(os.path.join(spritepath, 'side1.png'))
side2 = pg.image.load(os.path.join(spritepath, 'side2.png'))
side3 = pg.image.load(os.path.join(spritepath, 'side3.png'))
side4 = pg.image.load(os.path.join(spritepath, 'side4.png'))
side5 = pg.image.load(os.path.join(spritepath, 'side5.png'))
side6 = pg.image.load(os.path.join(spritepath, 'side6.png'))
side_pic = [side1, side2, side3, side4, side5, side6]

frame1 = Frame(slide1,0,0,40)
frame2 = Frame(slide2,0,0,40)
frame2_item1 = Subframe(frame2, slide2_item1, 35, 50)
frame2_item2 = Subframe(frame2, slide2_item2, 35, 150)
frame2_item3 = Subframe(frame2, slide2_item3, 340, 150)
frame2_item4 = Subframe(frame2, slide2_item4, 250, 465)
side_menu = pushSlide(frame2, side_pic[0], side_pic, -145, 0, -10, 0, 5,20)

jam_subuh = print_teks(frame2, f'{subuh}', 170, 236, 'comicsans', 30, False, False, (0,0,0))
jam_dzuhur = print_teks(frame2, f'{dzuhur}', 170, 276, 'comicsans', 30, False, False, (0,0,0))
jam_ashar = print_teks(frame2, f'{ashar}', 170, 316, 'comicsans', 30, False, False, (0,0,0))
jam_maghrib = print_teks(frame2, f'{maghrib}', 170, 356, 'comicsans', 30, False, False, (0,0,0))
jam_isya = print_teks(frame2, f'{isya}', 170, 396, 'comicsans', 30, False, False, (0,0,0))

salamStat = False
switch_slide_stat = False
start_time = time.time()
t = 0
while run:
    clock.tick()
    t += 1
    if t >= 120:
        switch_slide(frame1, frame2)
    if t == 50:
        play_salam()
    for event in pg.event.get():
        if event.type == pg.QUIT:
            run = False
        pos = pg.mouse.get_pos()
        click = pg.mouse.get_pressed()

    side_menu.mouseX, side_menu.mouseY = pos
    side_menu.mouseClick = click[0]

    drawWindow()
    pg.display.update()

pg.quit()
#sys.exit()