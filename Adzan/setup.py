import cx_Freeze

executables = [cx_Freeze.Executable("adzan1.py", base='Win32GUI')]

cx_Freeze.setup(
    name="A bit Racey",
    options={"build_exe": {"packages":["pygame"],
                           }},
    executables = executables

    )